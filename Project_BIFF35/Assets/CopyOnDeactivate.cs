﻿using UnityEngine;
using System.Collections;

public class CopyOnDeactivate : MonoBehaviour {
    public GameObject _targetToCopy;
    public SpriteRenderer _sprite;

    public bool _wasOn;

    public void Update() {

        if (_sprite.enabled == false & _wasOn==true) {


            GameObject gm = GameObject.Instantiate(_targetToCopy);
            Destroy(gm, 30f);
            _wasOn = false;
            gm.GetComponent<SpriteRenderer>().enabled = true;
            Destroy(this);
        }
        _wasOn = _sprite.enabled;

    }
}

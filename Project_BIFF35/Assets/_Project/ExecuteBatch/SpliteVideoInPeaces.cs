﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System;
using System.IO;

public class SpliteVideoInPeaces : MonoBehaviour {

    public string _ffmpegPath = "ffmpeg";
    public string _batchFilePath =  "/_Project/Batch/SplitVideo.bat";
    public string _videoPath;
    public string _exportPath;
    public float _timeOfPeaces = 5f;
    public float _videoDuration = 360f;
    public string _fileExportType = ".mp4";
    // Use this for initialization
    void Start () {
        StartCoroutine( SplitVideo(_videoPath, _exportPath, _timeOfPeaces, _videoDuration));
	}
    
    public IEnumerator SplitVideo(string videoToSplitPath, string directoryExportPath, float timeOfSplit , float maxTimeVideo) {

        if (!File.Exists(videoToSplitPath))
        {
            UnityEngine.Debug.Log("No Video File: " + videoToSplitPath);
            yield break;
        }
        string batchPath = Application.dataPath + "/_Project/Batch/SplitVideo.bat";
        if (!File.Exists(batchPath))
        {
            UnityEngine.Debug.Log("No Batch File: " + batchPath);
            yield break;
        }
        string fileName = Path.GetFileNameWithoutExtension(videoToSplitPath);
        if(Directory.Exists(directoryExportPath))
            Directory.Delete(directoryExportPath,true);
        Directory.CreateDirectory(directoryExportPath);
        
        bool finish=false;
        int i = 0;
        while ( ! finish) {
            float from = ((float)i) * timeOfSplit;
            float duration = timeOfSplit;

            finish = from + duration > maxTimeVideo;
            if (finish)
                duration = maxTimeVideo - from;
            if (duration == 0f) {
                yield break;
            }
            //::formPath toPath time(00:00:00.0) delay
            System.Diagnostics.Process.Start(_batchFilePath,string.Format("{0} {1} {2} {3} {4}", _ffmpegPath, videoToSplitPath, directoryExportPath +"/"+i+ "_"+fileName+ _fileExportType, GetStringTimer(from), duration));
           
            i++;
            print("Processing: "+ ( from / maxTimeVideo )+ "%");
            yield return new WaitForSeconds(1);
        }
        //        File videoToSplit = File.



    }

    private string GetStringTimer(float from)
    {
        System.TimeSpan t = System.TimeSpan.FromSeconds(from);
        return string.Format("{0:D2}:{1:D2}:{2:D2}.{3:D3}", t.Hours, t.Minutes, t.Seconds, t.Milliseconds);

    }

    public void ExecuteCommand(string command)
    {
        string batchPath = Application.dataPath + "/_Project/Batch/SplitVideo.bat";
        print("BatExiste:"+ File.Exists( batchPath));
       // ExecuteBatch(batchPath);

        //int ExitCode;
        //ProcessStartInfo ProcessInfo;
        //Process process;
        //ProcessInfo = new ProcessStartInfo(Application.dataPath + "/_Project/Batch/SplitVideo.bat", command);
        //ProcessInfo.CreateNoWindow = true;
        //ProcessInfo.UseShellExecute = false;
        //ProcessInfo.WorkingDirectory = Application.dataPath + "/_Project/Batch";
        //// *** Redirect the output ***
        //ProcessInfo.RedirectStandardError = true;
        //ProcessInfo.RedirectStandardOutput = true;

        //process = Process.Start(ProcessInfo);
        //process.WaitForExit();

        //// *** Read the streams ***
        //string output = process.StandardOutput.ReadToEnd();
        //string error = process.StandardError.ReadToEnd();

        //ExitCode = process.ExitCode;

        //print("output>>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
        //print("error>>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
        //print("ExitCode: " + ExitCode.ToString());
        //process.Close();
    }
}

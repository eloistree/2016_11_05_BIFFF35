﻿using UnityEngine;
using System.Collections;

public class StreamVideo : MonoBehaviour {

    public MediaPlayerCtrl _player;
    public MediaPlayerCtrl _player2;
    public string _path = "file:///";
    // Use this for initialization

    IEnumerator Start () {
        _player.DownloadStreamingVideoAndLoad2(_path);
        yield return new WaitForSeconds(1f);
        _player.Play();
    }
	
}

﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Linq;

public class ReaFiles : MonoBehaviour {

    public string _movieSplitedPath;
    public MediaPlayerCtrl _player;
    public MediaPlayerCtrl _player2;
    public GameObject _playerDisplay;
    public GameObject _player2Display;
    public List<string> _files;
    public float _timeBetween=1;
    void  Start () {
        StartCoroutine(LoadAndPlay());
    }
	
	// Update is called once per frame
	IEnumerator LoadAndPlay () {
        DirectoryInfo directory = new DirectoryInfo(_movieSplitedPath);
        foreach (FileInfo f in directory.GetFiles().OrderBy(fi => int.Parse(fi.Name.Split('_')[0])))
        {
            _files.Add(f.FullName);
        }
        //_player.load
        for (int i = 0; i < _files.Count; i++)
        {
            bool isPair = i % 2 == 0;
            MediaPlayerCtrl selectedToLoad = isPair ? _player : _player2;
            MediaPlayerCtrl selectedToPlay = isPair ? _player2 : _player;

            _playerDisplay.gameObject.SetActive(isPair);
            _player2Display.gameObject.SetActive(!isPair);
            string filePath = _files[i].Replace("\\", "//");


            selectedToPlay.Play();
            selectedToLoad.Stop();
            yield return selectedToLoad.DownloadStreamingVideoAndLoad2("file:///" + filePath);
            //if (!Saver.IsFileSaved())
            //    yield return new WaitForEndOfFrame();
           // yield return new WaitForSeconds(1f);
            Debug.Log("Hey ? " + i);
            float duratin = selectedToPlay.GetDuration();
            print(duratin);
            yield return new WaitForSeconds(((float)duratin)/1000f);// _player.GetDuration());
        }

    }
}

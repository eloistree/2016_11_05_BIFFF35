﻿using System.IO;
using System.Threading;
using UnityEngine;

public class Saver
{
    public static byte[] data;
    private static string path;
    public static bool _isFileSaved = true;
    public static bool IsFileSaved() { return _isFileSaved; }
    public static void Save(string filePath, byte[] dataToSave)
    {
        _isFileSaved = false;
        path = filePath;
        data = dataToSave;
        var thread = new Thread(SaveData);
        thread.Start();
    }

    private static void SaveData()
    {
        using (Stream file = File.Create(path))
        {
            CopyStream( new MemoryStream(data), file);
        }
        _isFileSaved = true;
    }

    public static void CopyStream(Stream input, Stream output)
    {
        byte[] buffer = new byte[8 * 1024];
        int len;
        while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
        {
            Thread.Sleep(100);
            output.Write(buffer, 0, len);
        }
        output.Flush();
        output.Close();
    }
}